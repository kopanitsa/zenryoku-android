package com.kopanitsa.zenryoku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by okadatakahiro on 2016/09/04.
 */

public class TestModel {

    @SerializedName("ping")
    private String ping;

    public String getPing() {
        return ping;
    }

    public void setPing(String ping) {
        this.ping = ping;
    }

}

