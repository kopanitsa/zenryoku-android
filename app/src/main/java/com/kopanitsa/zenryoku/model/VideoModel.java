package com.kopanitsa.zenryoku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by okadatakahiro on 2016/09/04.
 */

public class VideoModel {

//    VideoModel video = new VideoModel(id, current, version);

    @SerializedName("user_id")
    private String userId;

    @SerializedName("time_from")
    private String timeFrom;

    @SerializedName("version")
    private String version;

    public String getUserId() {
        return userId;
    }
    public String getTimeFrom() { return timeFrom;}
    public String getAppVersion() { return version;}

    public VideoModel (String userid, String timefrom, String version ) {
        this.userId = userid;
        this.timeFrom = timefrom;
        this.version = version;
    }
}

