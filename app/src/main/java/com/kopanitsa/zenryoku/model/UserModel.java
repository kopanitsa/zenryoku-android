package com.kopanitsa.zenryoku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by okadatakahiro on 2016/09/04.
 */

public class UserModel {

    @SerializedName("user_id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String data) {
        this.userId = data;
    }

    public UserModel (String userid) {
        this.userId = userid;
    }
}

