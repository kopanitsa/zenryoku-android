package com.kopanitsa.zenryoku.model;

import com.google.gson.annotations.SerializedName;
import com.kopanitsa.zenryoku.R;

/**
 * Created by okadatakahiro on 2016/09/04.
 */

public class PushModel {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("time_from")
    private String timeFrom;

    @SerializedName("file")
    private String file;

    public String getUserId() {
        return userId;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public int getFileRaw() {
        if (file.equals("1")) {
            return R.raw.v01;
        } else if (file.equals("2")) {
            return R.raw.v02;
        } else if (file.equals("3")) {
//            return R.raw.v03;
        } else if (file.equals("4")) {
//            return R.raw.v04;
        } else if (file.equals("5")) {
//            return R.raw.v05;
        }
        return R.raw.v01;
    }

}

