package com.kopanitsa.zenryoku.model;

import com.google.gson.annotations.SerializedName;
import com.kopanitsa.zenryoku.R;

/**
 * Created by okadatakahiro on 2016/09/04.
 */

public class StampModel {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("stamp")
    private String stamp;

    public StampModel(String id, String stamp){
        this.userId = id;
        this.stamp = stamp;
    }

    public String getUserId() {
        return userId;
    }

    public int getStampDrawable() {
        if (stamp.equals("1")) {
            return R.drawable.stamp1;
        } else if (stamp.equals("2")) {
            return R.drawable.stamp2;
        } else if (stamp.equals("3")) {
            return R.drawable.stamp3;
        }
        return R.drawable.stamp1;
    }

}

