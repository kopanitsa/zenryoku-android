package com.kopanitsa.zenryoku;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.kopanitsa.zenryoku.model.PushModel;
import com.kopanitsa.zenryoku.model.StampModel;
import com.kopanitsa.zenryoku.model.TestModel;
import com.kopanitsa.zenryoku.model.UserModel;
import com.kopanitsa.zenryoku.model.VideoModel;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Random;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ZenryokuActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;

    private final static String TAG = ZenryokuActivity.class.getSimpleName();
    private final static String SERVER_IP = "http://192.168.11.4:3000";
    //private final static String SERVER_IP = "http://ec2-52-192-30-16.ap-northeast-1.compute.amazonaws.com:3000";
    private final static String URL_START = "/start";
    private final static String URL_STOP = "/stop";
    private final static String URL_TESTS = "/tests";
    private final static String URL_USERS = "/users";
    private final static String URL_VIDEOS = "/videos";
    private final static String URL_VIDEOS_ACK = "/videos/ack";
    private final static String URL_STAMPS = "/stamps";
    private final static String PUSHER_KEY = "3100037f2b016cb76772";
    private final static String PUSHER_CLUSTER = "ap1";
    private final static String PUSHER_CHANNEL = "zenryoku_channel";
    private final static String PUSHER_EVENT = "next_event";
    private final static String PUSHER_STAMP_EVENT = "stamp_event";
    private final static String APP_VERSION = "1.0";

    private Context mContext;
    private FrameLayout mBaseView;
    private SurfaceView mSurfaceView;
    private ImageView mPauseImageView;
    private SurfaceHolder mHolder;
    private MediaPlayer mMediaPlayer;
    private PowerManager.WakeLock mWakeLock;

    private Gson mGson = new Gson();
    private UserModel mUserIdModel;

    private ImageButton mStampButton1;
    private ImageButton mStampButton2;
    private ImageButton mStampButton3;
    private Button mStartButton;
    private Button mStopButton;

    private static final String SPNAME = "SP_DATA";
    private int mUserId = -1;  // TODO remove. mUserIdModelにマージできそう

    private Point mTouched = new Point();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_zenryoku);
        mContext = this;
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.hide();
        }

        mBaseView = (FrameLayout) findViewById(R.id.baseView);
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceview);
        mPauseImageView = (ImageView) findViewById(R.id.pause_image);
        mPauseImageView.setVisibility(View.VISIBLE);

        // SurfaceViewにコールバックを設定
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);

        // [GET] /testしてサーバへのアクセスを確認する
        checkServerConnection();

        // [GET] /userしてユーザ番号つける
        generateUser();

        // pusher
        PusherOptions options = new PusherOptions();
        options.setCluster(PUSHER_CLUSTER);
        Pusher pusher = new Pusher(PUSHER_KEY, options);
        pusher.connect();
        Channel channel = pusher.subscribe(PUSHER_CHANNEL);
        channel.bind(PUSHER_EVENT, new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                Log.e(TAG, "" + data);
                PushModel push = mGson.fromJson(data, PushModel.class);
                handleMovie(push);
            }
        });

        channel.bind(PUSHER_STAMP_EVENT, new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                Log.e(TAG, "" + data);
                try {
                    if (mMediaPlayer!=null && mMediaPlayer.isPlaying()) {
                        StampModel stamp = mGson.fromJson(data, StampModel.class);
                        handleStamp(stamp);
                    }
                } catch (Exception e) {
                }
            }
        });

        mStampButton1 = (ImageButton) findViewById(R.id.button1);
        mStampButton1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                postStamp(1);
            }
        });
        mStampButton2 = (ImageButton) findViewById(R.id.button2);
        mStampButton2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                postStamp(2);
            }
        });
        mStampButton3 = (ImageButton) findViewById(R.id.button3);
        mStampButton3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                postStamp(3);
            }
        });
        mStartButton = (Button) findViewById(R.id.button_start);
        mStartButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                startServer();
            }
        });
        mStopButton = (Button) findViewById(R.id.button_stop);
        mStopButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopServer();
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onResume() {
        super.onResume();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, TAG);
        mWakeLock.acquire();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWakeLock.release();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTouched.set((int) (event.getX()), (int) (event.getY()));
                break;
            case MotionEvent.ACTION_MOVE:
                double diffX = (double) ((event.getX() - mTouched.x));
                double diffY = (double) ((event.getY() - mTouched.y));
                int dist = (int) (Math.pow(diffX, 2.0) + Math.pow(diffY, 2.0));
                // mMediaPlayerのIllegalStateExceptionを防ぐ
                try {
                    if (dist > 160000 && mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                        Log.e(TAG, "next!!");
                        mMediaPlayer.pause();
                        postToDoneMovie();
                        mPauseImageView.setVisibility(View.VISIBLE);
                    }
                    int left = (int) diffX;
                    int top = (int) diffY;
                    mSurfaceView.layout(left, top, left + mSurfaceView.getWidth(), top
                            + mSurfaceView.getHeight());
                } catch (IllegalStateException e) {
                }

                break;
            case MotionEvent.ACTION_UP:
                mSurfaceView.layout(0, 0, mSurfaceView.getWidth(), mSurfaceView.getHeight());
                break;
        }

        return super.onTouchEvent(event);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
//        try {
//            mMediaPlayer = MediaPlayer.create(mContext, R.raw.v01);
//            mMediaPlayer.setDisplay(mHolder);
//            mMediaPlayer.setLooping(true);
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        ViewGroup.LayoutParams layoutParams = mSurfaceView.getLayoutParams();
        float ratio = (float) (1920.0 / 1080.0);
        layoutParams.width = (int) (mSurfaceView.getHeight() * ratio);
        mSurfaceView.setLayoutParams(layoutParams);
    }


    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        if (mMediaPlayer != null) {
//            mMediaPlayer.stop();
//            mMediaPlayer.release();
//            mMediaPlayer = null;
//        }
    }

    private void startServer() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_START)
                        .get()
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                }
                return result;
            }
        }.execute();
    }

    private void stopServer() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_STOP)
                        .get()
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                }
                return result;
            }
        }.execute();
    }

    private void checkServerConnection() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_TESTS)
                        .get()
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    postToast("サーバに接続できません。再度試してください");
                } else {
                    Log.e(TAG, "[OK] server connection");
                    TestModel test = mGson.fromJson(result, TestModel.class);
                    Log.e(TAG, "result : " + test.getPing());
                }
            }
        }.execute();
    }

    private void generateUser() {

        SharedPreferences data = getSharedPreferences(SPNAME, Context.MODE_PRIVATE);
        int userId = data.getInt("user_id", -1);
        if (userId != -1) {
            mUserId = userId;
            mUserIdModel = new UserModel(((Integer) userId).toString());
            Log.e(TAG, "user ID is already registered. id : " + userId);
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                RequestBody requestBody = null;
                if (mUserIdModel != null) {
                    requestBody = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"), mGson.toJson(mUserIdModel));
                }
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_USERS)
                        .post(requestBody)
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("接続エラー");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("接続エラー");
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    postToast("接続エラー");
                } else {
                    Log.e(TAG, "[OK] generate user");
                    UserModel user = mGson.fromJson(result, UserModel.class);
                    SharedPreferences data = getSharedPreferences(SPNAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = data.edit();
                    editor.putInt("user_id", Integer.parseInt(user.getUserId()));
                    editor.apply();
                    Log.e(TAG, "result : " + result);
                    mUserId = Integer.parseInt(user.getUserId());
                    mUserIdModel = user;
                    Log.e(TAG, "user ID is registered/confirmed now. id : " + mUserId);
                }
            }
        }.execute();

    }

    // 次の再生者に渡すデータを詰めてPOST
    private void postToDoneMovie() {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;

                Float t = (float) ((float) (mMediaPlayer.getCurrentPosition()) / 1000.0);
                VideoModel video = new VideoModel(((Integer) mUserId).toString(), t.toString(), APP_VERSION);
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse("application/json; charset=utf-8"), mGson.toJson(video));
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_VIDEOS)
                        .post(requestBody)
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("接続エラー");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("接続エラー");
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    postToast("接続エラー");
                } else {
                    Log.e(TAG, "[OK] post player is paused");
                    Log.e(TAG, "result : " + result);
                }
                mMediaPlayer.stop();
                mMediaPlayer.release();
            }
        }.execute();

    }

    private void postMovieStartAck() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse("application/json; charset=utf-8"), mGson.toJson(mUserIdModel));
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_VIDEOS_ACK)
                        .post(requestBody)
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("接続エラー");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("接続エラー");
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    postToast("サーバに接続できません。再度試してください");
                } else {
                    Log.e(TAG, "[OK] server connection");
                    TestModel test = mGson.fromJson(result, TestModel.class);
                    Log.e(TAG, "result : " + test.getPing());
                }
            }
        }.execute();
    }

    private void postToast(final String message) {
        Handler h = new Handler(((Activity) mContext).getApplication().getMainLooper());
        h.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void postStamp(final Integer StampNum) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                StampModel stamp = new StampModel("", StampNum.toString());
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse("application/json; charset=utf-8"), mGson.toJson(stamp));
                Request request = new Request.Builder()
                        .url(SERVER_IP + URL_STAMPS)
                        .post(requestBody)
                        .build();
                OkHttpClient client = new OkHttpClient();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    postToast("サーバに接続できません。再度試してください");
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    postToast("サーバに接続できません。再度試してください");
                } else {
                    Log.e(TAG, "[OK] sent stamp");
                }
            }
        }.execute();

    }


    // 自分に向けての指示だったら、seekして動画をstartする。
    private void handleMovie(PushModel push) {
        if (Integer.parseInt(push.getUserId()) == mUserId) {
            Log.e(TAG, "start movie! file:" + push.getFileRaw() + " time:" + push.getTimeFrom());

            // serverにackを返す
            if (mUserIdModel != null) {
                Log.e(TAG, "handle movie. return ack to server");
                postMovieStartAck();
            }

            Handler h = new Handler(((Activity) mContext).getApplication().getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    mPauseImageView.setVisibility(View.INVISIBLE);
                }
            });


            mMediaPlayer = new MediaPlayer();
            try {
                String fileName = "android.resource://" + getPackageName() + "/" + push.getFileRaw();
                mMediaPlayer.setDataSource(this, Uri.parse(fileName));
                mMediaPlayer.setDisplay(mHolder);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });
                mMediaPlayer.prepareAsync();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 自分が動画再生してたら、スタンプ降らせる
    private void handleStamp(final StampModel stamp) {
        Handler h = new Handler(((Activity) mContext).getApplication().getMainLooper());
        h.post(new Runnable() {
            @Override
            public void run() {
                final ImageView imageView = new ImageView(mContext);
                imageView.setImageResource(stamp.getStampDrawable());
                Random r = new Random();
                // サイズ調整
                int s = r.nextInt(150);
                int size = s + 50;
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(size, size);
                imageView.setLayoutParams(layoutParams);
                // 表示
                mBaseView.addView(imageView);
                // アニメーション
                int px = r.nextInt(mSurfaceView.getWidth());
                TranslateAnimation translate = new TranslateAnimation(px, px, -size, mSurfaceView.getHeight() + 100);
                translate.setDuration(2000);
                imageView.startAnimation(translate);

                // 落ち終わったら破棄
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBaseView.removeView(imageView);
                    }
                }, 2000);
            }
        });
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Zenryoku Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://host/path"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient.connect();
        AppIndex.AppIndexApi.start(mClient, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mClient, getIndexApiAction());
        mClient.disconnect();
    }
}
